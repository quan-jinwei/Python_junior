'''
    作者：韩迪
    功能：BMR计算器
    版本：4.0
    日期：04/11/2022
    3.0 增加功能：用户可以在一行输入所有信息，带单位的信息输出
    4.0 增加功能：增加循环
'''


y_or_n = input('是否退出程序(y/n)？')

while y_or_n != 'y':
    height, weight = eval(input("请输入身高(米)和体重(公斤)[逗号隔开]: "))
    bmi = weight / pow(height, 2)
    print("BMI 数值为：{:.2f}".format(bmi))
    who, nat = "", ""
    if bmi < 18.5:
        who, nat = "偏瘦", "偏瘦"
    elif 18.5 <= bmi < 24:
        who, nat = "正常", "正常"
    elif 24 <= bmi < 25:
        who, nat = "正常", "偏胖"
    elif 25 <= bmi < 28:
        who, nat = "偏胖", "偏胖"
    elif 28 <= bmi < 30:
        who, nat = "偏胖", "肥胖"
    else:
        who, nat = "肥胖", "肥胖"
    print("BMI 指标为:国际'{0}', 国内'{1}'".format(who, nat))
    y_or_n = input('是否退出程序(y/n)？')

