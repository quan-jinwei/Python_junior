'''
功能：模拟掷骰子
版本：4.0
'''
import random
import matplotlib.pyplot as plt

# 解决中文显示问题
plt.rcParams['font.sans-serif'] = ['SimHei']  # SimHei黑体
#plt.rcParams['axes.unicode_minus'] = False


def roll_dice():
    roll = random.randint(1, 6)
    return roll


def main():
    total_times = 100
    # 记录骰子的结果
    roll_list = []

    for i in range(total_times):
        roll1 = roll_dice()
        roll2 = roll_dice()
        roll_list.append(roll1 + roll2)
    # 数据可视化
    plt.hist(roll_list, bins=range(2, 14), density=1, edgecolor='black', linewidth=1)
    # edgeclor:边缘颜色   linewidth:边缘宽度  normed=1时转化为概率图
    plt.title('骰子点数统计')  # 名称
    plt.xlabel('点数')
    plt.ylabel('频率')

    plt.show()


if __name__ == '__main__':
    main()