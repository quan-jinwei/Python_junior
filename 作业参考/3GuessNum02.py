from random import *
x = randint(0, 9)
print("{0:*^50}".format("猜数字游戏"))
for i in range(10):

    try:
        y = eval(input("请输入一个0到9的数:"))
        if x < y:
            print("遗憾，太大了")
        elif x > y:
            print("遗憾，太小了")
        else:
            print("预测" + str(i + 1) + "次,""恭喜你，你猜中了！")
            break
    except NameError:
        print("输入格式有误！")
    except SyntaxError:
        print("不支持该输入类型")
    else:
        print("没关系，继续加油！")
    finally:
        print("这是第{}次猜".format(i + 1))
print("{0:*^30}".format("游戏结束"))
