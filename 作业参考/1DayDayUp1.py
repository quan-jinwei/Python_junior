dayup = 1.0
for i in range(365):
    if i % 4 in [1, 2, 3]:
        dayup = dayup * (1 + 0.01)
    else:
        dayup = dayup * (1 - 0.001)
print("工作三天休息一天的力量:{:.3f} ".format(dayup))
