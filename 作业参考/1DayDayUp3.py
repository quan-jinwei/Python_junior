# 工作3天休息一天呢？如果努力期限内每天提高1%，休息期限内每天下降1‰
x = 1
y = 0.01
a = 'study'
b = 'unstudy'
temp = 1
state = a
for i in range(365):
    if state == a and (temp in [1, 2, 3]):
        x = x
        temp = (temp + 1) % 7
    elif state == a and (temp in [0, 4, 5, 6]):
        x = x * (1 + y)
        temp = (temp + 1) % 7
    elif state == b:
        temp = 0
print("365天后的能力值为：{:.3f}".format(x))
